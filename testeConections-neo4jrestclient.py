#!/usr/bin/env python
#coding:utf-8
from neo4jrestclient.client import GraphDatabase

def beginConnections():
    url = "http://neo4j.fabricadesoftware.ifc.edu.br:80/db/data/"
    connections = GraphDatabase(url, username="neo4j", password="neo4j.7@ifc")
    return connections

def insertLabel():
    connections = beginConnections()
    label = connections.labels.create("teste")
    label.create(
        name='Teste',
        age='29',
        email='giorgyismael@gmail.com',
    )

if __name__ == '__main__':
    insertLabel()
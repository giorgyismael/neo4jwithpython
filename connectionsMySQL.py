#!/usr/bin/env python
#coding:utf-8

import MySQLdb

def beginConnections():
    connections= MySQLdb.connect(host="localhost",user="root",
                      passwd="root",db="logradouros", port=3306)
    return connections.cursor()

def listPersons():
    cursor = beginConnections()
    listPersons = []

    cursor.execute('''select cpf, rg, nome,email, tel, cel,
                        log_cd_logradouro from cad_usuario''')
    for cpf,rg,nome,email,tel,cel,cod_logradouro in (cursor.fetchall()):
        person = {
            "cpf":cpf,
            "rg":rg,
            "name":nome.decode('latin-1').encode("utf-8").replace("\t", " "),
            "mail":email,
            "tel":tel,
            "cel":cel,
            "cod_address":cod_logradouro
        }
        listPersons.append(person)

    return listPersons

def listStates():
    cursor = beginConnections()
    listStates = []

    cursor.execute('''select * from uf''')
    for cod_uf, sigla_uf, nome_uf in (cursor.fetchall()):
        uf = {
            "cod_state":cod_uf,
            "sigla_state":sigla_uf,
            "name_state":nome_uf.decode('latin-1').encode("utf-8")
        }
        listStates.append(uf)

    return listStates

def listCities():
    cursor = beginConnections()
    listCities = []

    cursor.execute('''select uf_cd_uf, cd_cidade, ds_cidade_nome from cidades''')
    for cod_state, cod_city, nome_city in (cursor.fetchall()):
        city = {
            "cod_city":cod_city,
            "cod_state":cod_state,
            "name_city":nome_city.decode('latin-1').encode("utf-8").replace("\t", " "),
        }
        listCities.append(city)

    return listCities


def listDistrict():
    cursor = beginConnections()
    listDistrict = []

    cursor.execute('''select cd_bairro, cidade_cd_cidade, ds_bairro_nome from bairros''')
    for cod_dist, cod_city, nome_dist in (cursor.fetchall()):
        district = {
            "cod_dist":cod_dist,
            "cod_city":cod_city,
            "name_dist":nome_dist.decode('latin-1').encode("utf-8").replace("\t", " "),
        }
        listDistrict.append(district)

    return listDistrict

def listAddress():
    cursor = beginConnections()
    listAddress = []

    cursor.execute('''select cd_logradouro, bairros_cd_bairro, ds_logradouro_nome  from logradouro''')
    for cod_address, cod_dist, nome_address in (cursor.fetchall()):
        address = {
            "cod_address":cod_address,
            "cod_dist":cod_dist,
            "name_address":nome_address.decode('latin-1').encode("utf-8").replace("\t", " "),
        }
        listAddress.append(address)

    return listAddress

def listProducts():
    cursor = beginConnections()
    listProducts = []

    cursor.execute('''select cod_produto, descricao,preco_unit,qtd_produto from produto''')
    for cod_produto, descricao, preco_unit, qtd_produto  in (cursor.fetchall()):
        product = {
            "cod_product":cod_produto,
            "descript_product":descricao.decode('latin-1').encode("utf-8").replace("\t", " "),
            "price_product":preco_unit,
            "qtd_product":qtd_produto,
        }
        listAddress.append(product)

    return listProducts

def listProducts():
    cursor = beginConnections()
    listProducts = []

    cursor.execute('''select cod_produto, descricao,preco_unit,qtd_produto from produto''')
    for cod_produto, descricao, preco_unit, qtd_produto  in (cursor.fetchall()):
        product = {
            "cod_product":cod_produto,
            "descript_product":descricao.decode('latin-1').encode("utf-8").replace("\t", " "),
            "price_product":preco_unit,
            "qtd_product":qtd_produto,
        }
        listProducts.append(product)

    return listProducts

def listRequest():
    cursor_one = beginConnections()
    cursor_two = beginConnections()
    listRequest = []

    cursor_one.execute('''select cod_pedido, cad_usuario_cpf,dtped,faturado,naofaturado,dtentrega from pedidos ''')
    for cod_pedido, cpf_usuario, data_pedido, faturado, nao_faturado,data_entrega in (cursor_one.fetchall()):

        cursor_two.execute('''select prod_cod_produto from itemped where ped_codpedidos={} '''.format(cod_pedido))
        request = {
            "cod_request":cod_pedido,
            "cpf_person":cpf_usuario,
            "date_request":data_pedido,
            "billed":faturado,
            "not_billed":nao_faturado,
            "date_delivery":data_entrega,
            "cod_products":[cod_product[0] for cod_product in cursor_two.fetchall()],
        }
        listRequest.append(request)

    return listRequest

#Teste de conexão e Consultas
'''
if __name__ == '__main__':
    cursor = beginConnections()
    listPersons()
    listStates()
    listCities()
    listDistrict()
    listAddress()
    listProducts()
    listProducts()
'''
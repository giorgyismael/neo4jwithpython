#!/usr/bin/env python
#coding:utf-8

from neo4jrestclient.client import GraphDatabase
from neo4jrestclient.query import Q

url = "http://neo4j.fabricadesoftware.ifc.edu.br/db/data/"
conexao = GraphDatabase(url, username="neo4j", password="neo4j.7@ifc")

'''Pesquisa por String'''
pesquisa = Q("id", exact="e239e568bc0d2271bb6d3c806100e895")
node = conexao.nodes.filter(pesquisa)
print(len(node))
print(node[0])
print(node[0].properties)

'''Pesquisa por Inteiro'''
pesquisa = Q("age", equals=30)
node = conexao.nodes.filter(pesquisa)
print(len(node))
print(node[0])
print(node[0].properties)


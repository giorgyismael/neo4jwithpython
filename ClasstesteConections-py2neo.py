
'''
from py2neo import authenticate, Graph, Node, Relationship

authenticate("localhost:7474", "neo4j", "admin")
g = Graph("http://localhost:7474/db/data/")
tx = g.begin()
a = Node("Person", name="Alice")
tx.create(a)
b = Node("Person", name="Bob")
ab = Relationship(a, "KNOWS", b)
tx.create(ab)
tx.commit()

'''

from neo4jrestclient.client import GraphDatabase
from neo4jrestclient.query import Q

class Node():
    url = "http://localhost:7474/db/data/"
    connections = GraphDatabase(url, username="neo4j", password="admin")

    def createNode(self, label=None, **kwargs):
        if(label):
            label = self.connections.labels.create(label)
            node = label.create()
            node.properties = kwargs
        else:
            node = self.connections.nodes.create()
            node.properties = kwargs

    def createRelationship(self, nodeA, nodeB, nameRelationship):
        nodeA.relationships.create(nameRelationship, nodeB)

    def objects(self, atribute, value):
        print(atribute, value)
        #return self.connections.nodes.get(atribute=value)
        return self.connections.nodes.filter(Q("'%s'" %atribute, equals="'%s'" %value))
        #query = 'MATCH (n { %s: "%s" }) RETURN n' %(atribute, value)
        #return self.connections.query(query)[0]

'''
#coding:utf-8
from neo4jrestclient.client import GraphDatabase

#begin connections with autentications
url = "http://localhost:7474/db/data/"
gdb = GraphDatabase(url, username="neo4j", password="admin")

#create node
alice = gdb.nodes.create(name="Alice", age=30)
bob = gdb.nodes.create(name="Bob", age=30)
erick = gdb.node(name="Alice", age=30, color="Red")

#criar Label
person = gdb.labels.create("Person")
person.add(alice, bob, erick)

#criar node in label
carl = person.create(name="Carl", age=25)



#Criar Relacoes
carl.Knows(bob)
carl.Knows(erick)
alice.relationships.create("Knows", bob, since=1980)
bob.Knows(carl, since=123456789, introduced_at="Christmas party")


#operations with node

carl.properties = {"address":"ABC"} 'sobrescreve todas os atributos'
node = gdb.node[40] #get node
value = node['age'] #get value propertie
node['age'] = 15 #set propertie
node.set('address','ABC') #set new propertie
node.delete('name') #delete propertie


#print Properties
print(carl.properties)
print(carl.id)
print(carl.url)

#percorrer nodes
print(gdb.nodes.all())
for i in range(len(gdb.nodes.all())):
    print(gdb.node[i])

#percorrer label
person = gdb.labels.get("Person")
for i in person.get(age=25):
    print(gdb.node[i])

'''
'''
    #pesquisas de nodes e labels para realizar relationship
    connections = beginConnections()
    query = 'MATCH (n { name_state: "Santa Catarina" }) RETURN n'
    lookup = Q("cod_state", equals=22)
    result = connections.nodes.filter(lookup)

    for i in result:
        print(i.properties)


    for i in result:
        print(i.properties)

    node = connections.labels.get("States").get(name_state="Santa Catarina")
    for i in node:
        print(i)


    for i in (connections.query(query)[0].get('data').get('cod_state')):
        print(i[0].get('data').get('cod_state'))

'''
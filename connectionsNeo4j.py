#!/usr/bin/env python
#coding:utf-8

from connectionsMySQL import listStates, listCities, listDistrict, listAddress, listPersons, listProducts, listRequest
from neo4jrestclient.client import GraphDatabase
from neo4jrestclient.query import Q


def beginConnections():
    url = "http://localhost:7474/db/data/"
    connections = GraphDatabase(url, username="neo4j", password="admin")
    return connections

def insertStates():
    connections = beginConnections()
    labelState = connections.labels.create("States")

    relationOfStates = listStates()
    for state in relationOfStates:
        labelState.create(
          name_state=state.get('name_state'),
          cod_state=state.get('cod_state'),
          sigla_state=state.get('sigla_state'),

        )
        print(state.get('name_state'))

def insertCities():
    connections = beginConnections()
    labelCity = connections.labels.create("City")

    for city in listCities():

        nodeCity = labelCity.create(
              name_city=city.get('name_city'),
              cod_city=city.get('cod_city'),
              cod_state=city.get('cod_state'),

            )
        print(city.get('name_city'))
        nodeRelationships = connections.nodes.filter(Q("cod_state", equals=city.get('cod_state')))[0]
        nodeCity.relationships.create("PERTENCE_AO_ESTADO", nodeRelationships)

def insertDistricts():
    connections = beginConnections()
    labelDistrict = connections.labels.create("District")

    for district in listDistrict():

        nodeDistrick = labelDistrict.create(
              cod_dist=district.get('cod_dist'),
              cod_city=district.get('cod_city'),
              name_dist=district.get('name_dist'),

            )
        print(district.get('name_dist'))

        nodeRelationshipsCity = connections.nodes.filter(Q("cod_city", equals=district.get('cod_city')))[0]
        nodeDistrick.relationships.create("PERTENCE_A_CIDADE", nodeRelationshipsCity)

        nodeRelationshipsState = connections.nodes.filter(Q("cod_state", equals=nodeRelationshipsCity.get('cod_state')))[0]
        nodeDistrick.relationships.create("PERTENCE_AO_ESTADO", nodeRelationshipsState)

def insertAddress():
    connections = beginConnections()
    labelAddress = connections.labels.create("Address")

    for address in listAddress():

        nodeAddress = labelAddress.create(
              cod_address=address.get('cod_address'),
              cod_dist=address.get('cod_dist'),
              name_address=address.get('name_address'),

            )
        print(address.get('name_address'))

        nodeRelationshipsDistrict = connections.nodes.filter(Q("cod_dist", equals=address.get('cod_dist')))[0]
        nodeAddress.relationships.create("PERTENCE_AO_BAIRRO", nodeRelationshipsDistrict)

        nodeRelationshipsCity = connections.nodes.filter(Q("cod_city", equals=nodeRelationshipsDistrict.get('cod_city')))[0]
        nodeAddress.relationships.create("PERTENCE_A_CIDADE", nodeRelationshipsCity)

        nodeRelationshipsState = connections.nodes.filter(Q("cod_state", equals=nodeRelationshipsCity.get('cod_state')))[0]
        nodeAddress.relationships.create("PERTENCE_AO_ESTADO", nodeRelationshipsState)

def insertPersons():
    connections = beginConnections()
    labelPerson = connections.labels.create("Person")

    for person in listPersons():

        nodePerson = labelPerson.create(
              cpf=person.get('cpf'),
              rg=person.get('rg'),
              name=person.get('name'),
              mail=person.get('mail'),
              tel=person.get('tel'),
              cel=person.get('cel'),
              cod_address=person.get('cod_address'),

            )
        print(person.get('name'))

        nodeRelationshipsAddress = connections.nodes.filter(Q("cod_address", equals=person.get('cod_address')))[0]
        nodePerson.relationships.create("PERTENCE_AO_LOGRADOURO", nodeRelationshipsAddress)

        nodeRelationshipsDistrict = connections.nodes.filter(Q("cod_dist", equals=nodeRelationshipsAddress.get('cod_dist')))[0]
        nodePerson.relationships.create("PERTENCE_AO_BAIRRO", nodeRelationshipsDistrict)

        nodeRelationshipsCity = connections.nodes.filter(Q("cod_city", equals=nodeRelationshipsDistrict.get('cod_city')))[0]
        nodePerson.relationships.create("PERTENCE_A_CIDADE", nodeRelationshipsCity)

        nodeRelationshipsState = connections.nodes.filter(Q("cod_state", equals=nodeRelationshipsCity.get('cod_state')))[0]
        nodePerson.relationships.create("PERTENCE_AO_ESTADO", nodeRelationshipsState)


def insertProducts():
    connections = beginConnections()
    labelProduct = connections.labels.create("Products")

    for product in listProducts():
        labelProduct.create(
          cod_product=product.get('cod_product'),
          descript_product=product.get('descript_product'),
          price_product=product.get('price_product'),
          qtd_product=product.get('qtd_product'),

        )
        print(product.get('descript_product'))

def insertRequests():
    connections = beginConnections()
    labelPedidos = connections.labels.create("Requests")

    for pedido in listRequest():

        nodePedido = labelPedidos.create(
              cod_request=pedido.get('cod_request'),
              cpf_person=pedido.get('cpf_person'),
              date_request=pedido.get('date_request'),
              billed=pedido.get('billed'),
              not_billed=pedido.get('not_billed'),
              date_delivery=pedido.get('date_delivery'),
              cod_products=pedido.get('cod_products'),

            )
        print(pedido.get('cpf_person'))

        nodeRelationshipsPerson = connections.nodes.filter(Q("cpf", iexact=pedido.get('cpf_person')))[0]
        nodeRelationshipsPerson.relationships.create("REALIZOU_PEDIDO", nodePedido)

        for cod_product in pedido.get('cod_products'):
            nodeRelationshipsProduct = connections.nodes.filter(Q("cod_product", equals=cod_product))[0]
            nodePedido.relationships.create("PERTENCE_AO_PEDIDO", nodeRelationshipsProduct)

            nodeRelationshipsPerson.relationships.create("COMPROU_PRODUTO", nodeRelationshipsProduct)

if __name__ == '__main__':
    #insertStates()
    #insertCities()
    #insertDistricts()
    #insertAddress()
    #insertPersons()
    #insertProducts()
    #insertRequests()
'''
    url = "http://localhost:7474/db/data/"
    gdb = GraphDatabase(url, username="neo4j", password="admin")
    address = gdb.labels.get("Address")

    i = 0
    for n in address.all():
        i +=1
        print(n.properties)
    #print(n.properties)
    #print(i)
    #print(address.get(cod_address=20170)[0])

'''
